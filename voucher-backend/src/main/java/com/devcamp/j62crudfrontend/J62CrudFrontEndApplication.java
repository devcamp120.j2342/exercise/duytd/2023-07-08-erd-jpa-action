package com.devcamp.j62crudfrontend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class J62CrudFrontEndApplication {

	public static void main(String[] args) {
		SpringApplication.run(J62CrudFrontEndApplication.class, args);
	}

}
