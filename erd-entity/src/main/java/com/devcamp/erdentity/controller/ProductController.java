package com.devcamp.erdentity.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.erdentity.entity.Products;
import com.devcamp.erdentity.repository.ProductRepository;

@RestController
@RequestMapping("/")
public class ProductController {
    
   @Autowired
   ProductRepository productRepository;

   @CrossOrigin
   @PostMapping("/products")
   public ResponseEntity<Object> createProducts(@RequestBody Products product) {
    try {
        Products newRole = new Products();
        newRole.setBuy_price(product.getBuy_price());
        newRole.setProduct(product.getProduct());
        newRole.setProduct_code(product.getProduct_code());
        newRole.setProduct_description(product.getProduct_description());
        newRole.setProduct_name(product.getProduct_name());
        newRole.setProduct_scale(product.getProduct_scale());
        newRole.setQuantity_in_stock(product.getQuantity_in_stock());
        Products saveProduct = productRepository.save(newRole);
        return new ResponseEntity<>(saveProduct, HttpStatus.CREATED);
    } catch(Exception e) {
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
   }

   @CrossOrigin
   @PutMapping("/product/{id}")
   public ResponseEntity<Object> updateProduct(@PathVariable("id") Integer id, @RequestBody Products product){
    Optional<Products> productDate = productRepository.findById(id);
    try {
        if (productDate.isPresent()){
            Products newProduct = productDate.get();
            newProduct.setBuy_price(product.getBuy_price());
            newProduct.setProduct(product.getProduct());
            newProduct.setProduct_code(product.getProduct_code());
            newProduct.setProduct_description(product.getProduct_description());
            newProduct.setProduct_name(product.getProduct_name());
            newProduct.setProduct_scale(product.getProduct_scale());
            newProduct.setProduct_vendor(product.getProduct_vendor());
            newProduct.setQuantity_in_stock(product.getQuantity_in_stock());
            Products saveProduct = productRepository.save(newProduct);
            return new ResponseEntity<>(saveProduct, HttpStatus.OK);
        }else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    } catch(Exception e) {
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
   }

   @CrossOrigin
   @DeleteMapping("/product/{id}")
   public ResponseEntity<Object> deleteProductId(@PathVariable("id")Integer id){
    try {
        Optional<Products> product = productRepository.findById(id);
        if(product.isPresent()){
            productRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    } catch(Exception e){
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
   }

   @CrossOrigin
   @DeleteMapping("/products")
   public ResponseEntity<Object> deleteProductAll(){
    try {
        productRepository.deleteAll();
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    } catch(Exception e) {
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
   }

   @CrossOrigin
   @GetMapping("/products")
   public List<Products> getAllProducts() {
        List<Products> productList = productRepository.findAll();    
        return productList;
   }

   @CrossOrigin
   @GetMapping("/products/{id}")
   public Products getProduct(@PathVariable("id") Integer id) {
        Optional<Products> productData = productRepository.findById(id);
        if(productData.isPresent()){
            Products product = productData.get();            
            return product;
        }else{
            return null;
        }
   }
}
