package com.devcamp.erdentity.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.erdentity.entity.Customer;
import com.devcamp.erdentity.repository.CustomerRepository;

@RestController
public class CustomerController {
    
    @Autowired
    CustomerRepository customerRepository;
        
    @GetMapping("/regions/{lastname}")
    public ResponseEntity<List<Customer>> getCustomerByLastName(@PathVariable("lastname") String lastname) {
        try {
            List<Customer> customers = customerRepository.findCustomerByLastNameLike(lastname, PageRequest.of(0, 10));
            return new ResponseEntity<>(customers, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);	
        }
    }


}
