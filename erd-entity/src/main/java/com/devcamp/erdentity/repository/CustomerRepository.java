package com.devcamp.erdentity.repository;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import java.util.*;

import com.devcamp.erdentity.entity.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Integer> {
    @Query(value = "FROM #{#entityName} WHERE last_name like ?1")
	List<Customer> findCustomerByLastNameLike(String customername);

	@Query(value = "SELECT * FROM Customer WHERE first_name like :para1 ORDER BY first_name DESC", nativeQuery = true)
	List<Customer> findCustomerByFirstNameDesc(@Param("para1") String name1);

    @Query(value = "FROM #{#entityName} WHERE last_name like ?1")
	List<Customer> findCustomerByLastNameLike(String lastname, PageRequest pageRequest);

    @Query("SELECT CONCAT(c.firstName, ' ', c.lastName) FROM Customer c WHERE CONCAT(c.firstName, ' ', c.lastName) LIKE %?1%")
    List<String> findFullNameLike(String keyword);

    @Query(value = "SELECT * FROM Customer WHERE city like :para1", nativeQuery = true)
    List<Customer> findCustomerByCityLike(@Param("para1") String city, PageRequest pageRequest);

    @Query(value = "SELECT * FROM Customer WHERE country like :para1 ORDER BY country DESC", nativeQuery = true)
    List<Customer> findCustomerByCountryLike(@Param("para1") String country, PageRequest pageRequest);

    @Modifying
    @Query(value = "UPDATE Customer SET country = NULL WHERE country = :param", nativeQuery = true)
    void updateCountryToNull(@Param("param") String country);

}
