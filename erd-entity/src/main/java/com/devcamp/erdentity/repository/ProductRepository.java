package com.devcamp.erdentity.repository;

import java.util.List;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.erdentity.entity.Products;

public interface ProductRepository extends JpaRepository<Products, Integer>{
    
    @Query(value = "SELECT * FROM Products WHERE product_name like :param", nativeQuery=true)
    List<Products> findProductsByProductNameLike(@Param("param") String productName);

    @Query(value = "SELECT * FROM Products WHERE product_code like :param ORDER BY product_code DESC", nativeQuery=true)
    List<Products> findProductsByProductCodeLike(@Param("param") String productCode, PageRequest pageRequest);

    @Modifying
    @Query(value = "UPDATE Products SET product_name = NULL WHERE product_name = :param", nativeQuery = true)
    void updateProductNameToNull(@Param("param") String productName);
}
